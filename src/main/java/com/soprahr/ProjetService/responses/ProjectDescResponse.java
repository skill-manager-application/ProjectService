package com.soprahr.ProjetService.responses;

import java.time.LocalDateTime;
import java.util.List;


public class ProjectDescResponse {
	private String id;
	private String projectName;
	private List<String> skillsId;
	private String status;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<String> getSkillsId() {
		return skillsId;
	}
	public void setSkillsId(List<String> skillsId) {
		this.skillsId = skillsId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDateTime getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}
	public LocalDateTime getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}
	
}
