package com.soprahr.ProjetService.responses;

import java.util.List;
public class TeamResponse {
	private String id;
	private List<String> usersId;
	private String status;
	private List<ProjectDescResponse> projects;
	public Object department;
	private String name;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getUsersId() {
		return usersId;
	}
	public void setUsersId(List<String> usersId) {
		this.usersId = usersId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ProjectDescResponse> getProjects() {
		return projects;
	}
	public void setProjects(List<ProjectDescResponse> projects) {
		this.projects = projects;
	}
	public Object getDepartment() {
		return department;
	}
	public void setDepartment(Object department) {
		this.department = department;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
