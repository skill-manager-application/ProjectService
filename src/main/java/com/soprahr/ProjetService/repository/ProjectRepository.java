package com.soprahr.ProjetService.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.soprahr.ProjetService.entities.Project;

public interface ProjectRepository extends MongoRepository<Project, String>{
}
