package com.soprahr.ProjetService.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.soprahr.ProjetService.entities.Team;

public interface TeamRepository extends MongoRepository<Team, String>{
	List<Team> findByDepartmentId(String departmentId);
}
