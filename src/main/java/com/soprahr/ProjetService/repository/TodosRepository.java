package com.soprahr.ProjetService.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.soprahr.ProjetService.entities.ToDo;

public interface TodosRepository extends MongoRepository<ToDo, String>{
	List<ToDo> findByProjectId(String projectId);
    List<ToDo> findByUserIds(String userId);
}
