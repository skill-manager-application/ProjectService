package com.soprahr.ProjetService.Controller;

import java.nio.file.AccessDeniedException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.ProjetService.Service.TodosService;
import com.soprahr.ProjetService.entities.ToDo;
import com.soprahr.ProjetService.repository.ProjectRepository;
import com.soprahr.ProjetService.repository.TeamRepository;
import com.soprahr.ProjetService.repository.TodosRepository;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/Todos")
public class TodosController {
	@Autowired
	TeamRepository teamRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	TodosRepository todosRepository;
	@Autowired
	TodosService todosService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ToDo>> getAllTeams() throws AccessDeniedException {
		//ModelMapper modelMapper = new ModelMapper();
		List<ToDo> teams = todosRepository.findAll();
		return new ResponseEntity<List<ToDo>>(teams,HttpStatus.OK);
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ToDo CreateTask(@RequestBody ToDo task) {
		return todosService.createTask(task);
	}
	@GetMapping(path="/{id}")
	public ToDo getProjectById(@PathVariable String id) {
		return todosRepository.findById(id).get();
	}
	
	@PutMapping
	public ToDo updateTodo(@RequestBody ToDo todo) {
		return todosRepository.save(todo);
	}
	
    // Delete Skill Based on it's id
	@DeleteMapping(path="/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable String id) {
		//Call SkillService to do the job
		todosService.deleteTask(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(path="/folder")
	public List<ToDo> getTodosByFolderId(@RequestParam(value="idProject") String idProject,@RequestParam(value="idFolder",defaultValue = "1")  String idFolder){
		return todosRepository.findByProjectId(idProject);
	}
	
	@GetMapping(path="/user/{id}")
	public List<ToDo> getTodosByUserId(@PathVariable String id){
		return todosService.getTodosByUserId(id);	
	}
	
	@GetMapping(path="/user/{projectId}/{userId}")
	public List<ToDo> getTodosByProjectIdAndUserId(@PathVariable String projectId,@PathVariable String userId){
		return todosService.getTodosByProjectIdAndUserId(projectId,userId);	
	}
	
	@GetMapping("/checklists/{userId}")
	public List<List<ToDo.checkList>> getValidatedChecklistsByUser(@PathVariable String userId) {
	    return todosService.getValidatedChecklistsByUser(userId);
	}
}
