package com.soprahr.ProjetService.Controller;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.soprahr.ProjetService.Service.TeamService;
import com.soprahr.ProjetService.entities.Team;
import com.soprahr.ProjetService.repository.TeamRepository;
import com.soprahr.ProjetService.responses.TeamResponse;



@CrossOrigin(origins="*")
@RestController
@RequestMapping("/Teams")
public class TeamController {
	
	@Autowired
	TeamRepository repository;
	@Autowired
	   RestTemplate restTemplate;
	@Autowired
	TeamService teamService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TeamResponse>> getAllTeams() throws AccessDeniedException {
		List<TeamResponse> ListteamResponse= new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();
		List<Team> teams = repository.findAll();
		for (Team team : teams) {
			TeamResponse teamResponse = modelMapper.map(team, TeamResponse.class);
			teamResponse.setDepartment(teamService.getDepartmentById(team.getDepartmentId()));
			ListteamResponse.add(teamResponse);
		}
		return new ResponseEntity<List<TeamResponse>>(ListteamResponse,HttpStatus.OK);
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Team CreateTeam(@RequestBody Team teamRequest) {
		return repository.save(teamRequest);
	}
	
	
	
	@GetMapping(path="/Department/{id}")
	public List<TeamResponse> getTeamsByDepartmentId(@PathVariable String id) {
		List<TeamResponse> ListteamResponse= new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();
		List<Team> teams = repository.findByDepartmentId(id);
		for (Team team : teams) {
			TeamResponse teamResponse = modelMapper.map(team, TeamResponse.class);
			ListteamResponse.add(teamResponse);
		}
		return ListteamResponse;
	}

	@GetMapping(path="/{id}")
	public TeamResponse getTeamById(@PathVariable String id) throws AccessDeniedException {
		Team team =  repository.findById(id).get();
		ModelMapper modelMapper = new ModelMapper();
		TeamResponse teamResponse = modelMapper.map(team, TeamResponse.class);
		teamResponse.setDepartment(teamService.getDepartmentById(team.getDepartmentId()));
		return teamResponse;
	}	
	@DeleteMapping(path="/{teamId}")
	public ResponseEntity<Object> deleteTeam(@PathVariable String teamId) {
		teamService.deleteTeam(teamId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(path="/user/{userId}")
	public List<Team> getTeamByUserId(@PathVariable String userId){
		return teamService.getTeamsByUserId(userId);
	}
	@PatchMapping(path="/user/{teamId}/{userId}")
	public ResponseEntity<Object> addOrRemoveUser(@PathVariable String teamId ,@PathVariable String userId) {
		teamService.addOrRemoveUser(teamId,userId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PatchMapping(path="/project/{teamId}/{projectId}")
	public ResponseEntity<Object> addOrRemoveProject(@PathVariable String teamId,@PathVariable String projectId) {
		teamService.addOrRemoveProjectFromTeam(teamId,projectId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PutMapping(path="{teamId}")
	public ResponseEntity<Object> updateTeam(@PathVariable String teamId,@RequestBody Team team){
		teamService.updateTeam(teamId,team);
		return new ResponseEntity<>(HttpStatus.OK);	}
	}
