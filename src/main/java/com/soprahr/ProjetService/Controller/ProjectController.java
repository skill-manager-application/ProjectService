package com.soprahr.ProjetService.Controller;

import java.nio.file.AccessDeniedException;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.ProjetService.Service.ProjectServices;
import com.soprahr.ProjetService.entities.Project;
import com.soprahr.ProjetService.entities.Project.Folder;
import com.soprahr.ProjetService.repository.ProjectRepository;
import com.soprahr.ProjetService.repository.TeamRepository;
import com.soprahr.ProjetService.repository.TodosRepository;
import com.soprahr.ProjetService.responses.ProjectResponse;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/Project")
public class ProjectController {
	@Autowired
	TeamRepository teamRepository;
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	ProjectServices projectServices;
	
	@Autowired
	TodosRepository	 todosRepository;
	
	// Merge Two Teams try it
	
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Project>> getAllProjects() throws AccessDeniedException {
		//ModelMapper modelMapper = new ModelMapper();
		List<Project> teams = projectRepository.findAll();
		teams.forEach((el)->{
			if(el.getTeam() != null) {
				el.getTeam().getProjects().forEach((ele)->{
					ele.setTeam(null);
				});
			}
		});
		return new ResponseEntity<List<Project>>(teams,HttpStatus.OK);
		
	}
	@PostMapping(path="/{teamId}",consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ProjectResponse CreateProject(@PathVariable String teamId, @RequestBody Project teamRequest) {
		Project p = projectServices.createProject(teamId, teamRequest);
		ProjectResponse projectResponse = new ProjectResponse();
		ModelMapper modelMapper = new ModelMapper();
		projectResponse = modelMapper.map(p, ProjectResponse.class);
		return projectResponse;
	}
	@GetMapping(path="/{id}")
	public ProjectResponse getProjectById(@PathVariable String id) {

		ProjectResponse projectResponse = new ProjectResponse();

		ModelMapper modelMapper = new ModelMapper();
		Project p = projectRepository.findById(id).get();
		projectResponse = modelMapper.map(p, ProjectResponse.class);
		return projectResponse;
	}
	
	@PatchMapping(path="/addfolder/{id}")
	public ProjectResponse addFolder(@PathVariable String id, @RequestBody Folder f) {
		Project p = projectServices.addFolderToProject(id,f);
		ModelMapper modelMapper = new ModelMapper();
		ProjectResponse projectResponse = modelMapper.map(p, ProjectResponse.class);
		return projectResponse;
	}
	
	@PatchMapping(path="/folder/{id}")
	public ProjectResponse updateFolder(@PathVariable String id, @RequestBody Folder f) {
		Project p = projectServices.updateFolder(id,f);
		ModelMapper modelMapper = new ModelMapper();
		ProjectResponse projectResponse = modelMapper.map(p, ProjectResponse.class);
		return projectResponse;
	}
	
	@PutMapping(path="/{projectId}")
	public ProjectResponse updateProject(@PathVariable String projectId,@RequestBody Project project) {
		Project p = projectServices.updateProject(projectId,project);
		ModelMapper modelMapper = new ModelMapper();
		ProjectResponse projectResponse = modelMapper.map(p, ProjectResponse.class);
		return projectResponse;
	}
    // Delete Skill Based on it's id
	@DeleteMapping(path="/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable String id) {
		//Call SkillService to do the job
		projectServices.deleteProject(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	//get project by user id
	@GetMapping(path="/user/{id}")
	public List<Project> getProjectsByUserId(@PathVariable String id){
		return projectServices.getProjectsByUserId(id);
	}
	//get project by superviserid id
	@GetMapping(path="/superviser/{id}")
	public List<Project> getProjectsBySuperviserrId(@PathVariable String id){
		return projectServices.getProjectsBySupervisorId(id);
	}
}
