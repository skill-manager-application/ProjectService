package com.soprahr.ProjetService.Service;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprahr.ProjetService.entities.Project;
import com.soprahr.ProjetService.entities.Team;
import com.soprahr.ProjetService.entities.ToDo;
import com.soprahr.ProjetService.entities.Project.Folder;
import com.soprahr.ProjetService.repository.ProjectRepository;
import com.soprahr.ProjetService.repository.TodosRepository;

@Service
public class TodosService {
	
	@Autowired
	TodosRepository todosRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	public ToDo createTask(ToDo task) {
		LocalDateTime now = LocalDateTime.now();
        Date date = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		task.setStartTime(date);
		ToDo newTask = todosRepository.save(task);
		Project project = projectRepository.findById(task.getProjectId()).get();
		project.getFolder().get(task.getFolderId()).addTask(newTask);
		projectRepository.save(project);
		return newTask;
	}
	
	public void deleteTask(String taskId) {
		todosRepository.deleteById(taskId);
	}
	
	
	public List<ToDo> getTodosByUserId(String userId) {
	    List<ToDo> userTodos = new ArrayList<>();
	    List<ToDo> todos = todosRepository.findAll();
	    for (ToDo todo : todos) {
	        if (todo.getUserIds().contains(userId)) {
	            userTodos.add(todo);
	        }
	    }

	    return userTodos;
	}
	
	public List<ToDo> getTodosByProjectIdAndUserId(String projectId, String userId) {
	    List<ToDo> matchingTodos = new ArrayList<>();
	    List<Project> projects = projectRepository.findAll();
	    for (Project project : projects) {
	        if (project.getId().equals(projectId)) {
	            Team team = project.getTeam();
	            if (team != null && team.getUsersId().contains(userId)) {
	                for (Folder folder : project.getFolder()) {
	                    for (ToDo todo : folder.getTodos()) {
	                        if (todo.getUserIds().contains(userId)) {
	                            matchingTodos.add(todo);
	                        }
	                    }
	                }
	            }
	            break;
	        }
	    }

	    return matchingTodos;
	}
	
	public List<List<ToDo.checkList>> getValidatedChecklistsByUser(String userId) {
	    List<ToDo> toDos = todosRepository.findByUserIds(userId);

	    List<List<ToDo.checkList>> validatedChecklistsByInterval = new ArrayList<>();

	    YearMonth currentMonth = YearMonth.now();
	    LocalDate firstDayOfMonth = currentMonth.atDay(1);
	    LocalDate lastDayOfMonth = currentMonth.atEndOfMonth();

	    // Divide the month into 5-day intervals
	    LocalDate startDate = firstDayOfMonth;
	    while (startDate.isBefore(lastDayOfMonth)) {
	        LocalDate endDate = startDate.plusDays(4).isBefore(lastDayOfMonth) ? startDate.plusDays(4) : lastDayOfMonth;
	        List<ToDo.checkList> checklistsForInterval = new ArrayList<>();

	        for (ToDo toDo : toDos) {
	            for (ToDo.checkList item : toDo.getCheckList()) {
	                if (item.getUserId().equals(userId) && item.isCheck()) {
	                    LocalDate itemDate = item.getUpdated().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	                    if (!itemDate.isBefore(startDate) && !itemDate.isAfter(endDate)) {
	                        checklistsForInterval.add(item);
	                    }
	                }
	            }
	        }

	        // Sort the checklists within the interval by the updated date
	        Collections.sort(checklistsForInterval, Comparator.comparing(ToDo.checkList::getUpdated));

	        validatedChecklistsByInterval.add(checklistsForInterval);
	        startDate = endDate.plusDays(1);
	    }

	    return validatedChecklistsByInterval;
	}
}
