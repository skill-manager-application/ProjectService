package com.soprahr.ProjetService.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.soprahr.ProjetService.entities.Project;
import com.soprahr.ProjetService.entities.Project.Folder;
import com.soprahr.ProjetService.entities.Team;
import com.soprahr.ProjetService.entities.ToDo;
import com.soprahr.ProjetService.repository.ProjectRepository;
import com.soprahr.ProjetService.repository.TeamRepository;

@Service
public class ProjectServices {
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	TeamRepository repository;
	
	
	//add folder
	public Project createProject(String teamId, Project p) {
		Team team = repository.findById(teamId).get();
		LocalDateTime now = LocalDateTime.now();
		p.setStartDate(now);
		p.setTeam(team);
		Project project = projectRepository.save(p);
		team.addProject(project);
		repository.save(team);
		return project;
	}
	
	public Project addFolderToProject(String id,Folder folder) {
		Project project = projectRepository.findById(id).get();
		project.addFolder(folder);
		return projectRepository.save(project);
	}
	public void deleteProject(String id) {
		projectRepository.deleteById(id);
	}
	
	public Project updateFolder(String id,Folder f) {
		Project project = projectRepository.findById(id).get();
		project.getFolder().forEach((el)->{
			if(f.getFolderNum() == el.getFolderNum()) {
				el.setTodos(f.getTodos());
				el.setFolderName(f.getFolderName());
				el.setStatus(f.getStatus());
				el.setGoals(f.getGoals());
			}
		});
		
		return projectRepository.save(project);
	}
	public Project updateProject(String projectId,Project project) {
		Project projectToUpdate = projectRepository.findById(projectId).get();
		projectToUpdate.setProjectName(project.getProjectName());
		projectToUpdate.setProjectDesc(project.getProjectDesc());
		return projectRepository.save(projectToUpdate);
	}
    
	public List<Project> getProjectsByUserId(String userId) {
	    List<Project> userProjects = new ArrayList<>();
	    List<Project> projects = projectRepository.findAll();
	    for (Project project : projects) {
	        if (project.getTeam().getUsersId().contains(userId)) {
	        	project.getTeam().setProjects(null);
	            userProjects.add(project);
	        }
	    }

	    return userProjects;
	}
	
	
	public List<Project> getProjectsBySupervisorId(String supervisorId) {
	    List<Project> supervisorProjects = new ArrayList<>();
	    List<Project> projects = projectRepository.findAll();

	    for (Project project : projects) {
	        List<Folder> folders = project.getFolder();
	        for (Folder folder : folders) {
	            List<ToDo> tasks = folder.getTodos();
	            for (ToDo task : tasks) {
	                if (task.getSupervisorId().equals(supervisorId)) {
	    	        	project.getTeam().setProjects(null);;
	                    supervisorProjects.add(project);
	                    break;
	                }
	            }
	        }
	    }

	    return supervisorProjects;
	}
	



}
