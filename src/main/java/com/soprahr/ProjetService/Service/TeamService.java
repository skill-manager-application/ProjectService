package com.soprahr.ProjetService.Service;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.soprahr.ProjetService.entities.Project;
import com.soprahr.ProjetService.entities.Team;
import com.soprahr.ProjetService.repository.ProjectRepository;
import com.soprahr.ProjetService.repository.TeamRepository;

@Service
public class TeamService {
	@Autowired
	   RestTemplate restTemplate;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	public Object getDepartmentById(String id) throws AccessDeniedException {
		
		HttpHeaders headers = new HttpHeaders();
		String token = "Bearer " + ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
		headers.set("Authorization", token);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		Object users = new Object();
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	       try {
		       users =  restTemplate.exchange("http://localhost:8080/api/departments/departmentDetails/"+id, HttpMethod.GET, entity, Object.class).getBody();
			}catch(Exception f) {
				throw new AccessDeniedException("Access dineid Please try to loggIn ");
			}
	       return users;
	}	

	
	public void deleteTeam(String id) {
		Team team = teamRepository.findById(id).get();
		team.getProjects().forEach((el)->{
			el.setTeam(null);
		});
		teamRepository.deleteById(id);
	}
	
	
	public List<Team> getTeamsByUserId(String userId) {
	    List<Team> matchingTeams = new ArrayList<>();
	    List<Team> teams = teamRepository.findAll();
	    for (Team team : teams) {
	        if (team.getUsersId().contains(userId)) {
	        	team.getProjects().forEach((el)->{
	        		el.setTeam(null);
	        	});
	            matchingTeams.add(team);
	        }
	    }

	    return matchingTeams;
	}

	public void addOrRemoveUser(String teamId, String userId) {
	    Team team = teamRepository.findById(teamId).get();
	    if (team != null) {
	        if (team.getUsersId().contains(userId)) {
	        	team.getUsersId().remove(userId);
	        } else {
	        	team.getUsersId().add(userId);
	        }
	        teamRepository.save(team); // Save the updated team
	    }
	}
	public boolean isProjectInTeam(String projectId, Team team) {
	    List<Project> projects = team.getProjects();
	    for (Project project : projects) {
	        if (project.getId().equals(projectId)) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void addOrRemoveProjectFromTeam(String teamId, String projectId) {
	    Team team = teamRepository.findById(teamId).get();
	    Project project = projectRepository.findById(projectId).get();
	    List<Team> teams = teamRepository.findAll();
	    // Retrieve the team with the given teamId
	    if (team != null) {
	        // Remove the new project from any previous team
	    	if(isProjectInTeam(projectId,team)) {
		    	team.removeProject(projectId);
	    	}else {
	    		project.setTeam(team);
	    		projectRepository.save(project);
	 	       	team.getProjects().add(project);
	    		teams.forEach((el)->{
	    			if(isProjectInTeam(projectId,team)) {
	    				el.removeProject(projectId);
	    		        teamRepository.save(el);
	    			}
	    		});
	    	}
	        teamRepository.save(team);
	    }
	}
	
	public void updateTeam(String teamId,Team team) {
	    Team updateTeam = teamRepository.findById(teamId).get();
	    updateTeam.setName(team.getName());
	    updateTeam.setStatus(team.getStatus());
        teamRepository.save(updateTeam); // Save the updated team

	}

}
