package com.soprahr.ProjetService.entities;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Team")
public class Team {
	
	@Id
	private String id;
	private String name;
	private List<String> usersId;
	private String status;
    @DBRef
	private List<Project> projects;
	private String departmentId;
	
	
	public Team(String id, String name, List<String> usersId, String status,
			List<String> skillsId, String departmentId) {
		super();
		this.id = id;
		this.name = name;
		this.usersId = usersId;
		this.status = status;
		this.projects = new ArrayList<>();
		this.departmentId = departmentId;
	}
	
	
	public Team() {
		super();
		this.projects = new ArrayList<>();

	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getUsersId() {
		return usersId;
	}
	public void setUsersId(List<String> usersId) {
		this.usersId = usersId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addProject(Project project) {
		if(this.projects.isEmpty()) {
			this.projects.add(0, project);
		}else{
			this.projects.add(project);
		}
	}
	public void removeProject(String projectId) {
		    this.projects.removeIf(project -> project.getId().equals(projectId));
	}
	
}
