package com.soprahr.ProjetService.entities;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Project")
public class Project {
	@Id
	private String id;
	private String projectName;
	private String projectDesc;
    @DBRef
	private Team team;
	private String status;
	private LocalDateTime startDate;
	private Date endDate;
	
	private List<Folder> folder = new ArrayList<>();	
	
	public LocalDateTime getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProjectDesc() {
		return projectDesc;
	}

	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public List<Folder> getFolder() {
		return folder;
	}
	public void setFolder(List<Folder> folder) {
		this.folder = folder;
	}

	public static class Folder {
		private int folderNum;
		private String folderName;
	    @DBRef
		private List<ToDo> todos = new ArrayList<>();
		private String goals;
		private String status;
		
		public String getFolderName() {
			return folderName;
		}
		public void setFolderName(String folderName) {
			this.folderName = folderName;
		}
		public List<ToDo> getTodos() {
			return todos;
		}
		public void setTodos(List<ToDo> todos) {
			this.todos = todos;
		}
		public String getGoals() {
			return goals;
		}
		public void setGoals(String goals) {
			this.goals = goals;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public int getFolderNum() {
			return folderNum;
		}
		public void setFolderNum(int folderNum) {
			this.folderNum = folderNum;
		}
		public void addTask(ToDo task) {
			if(this.todos.isEmpty()) {
				this.todos.add(0, task);
			}else {
				this.todos.add(task);
			}
		}
	}
	
	public void addFolder(Folder f) {
		if(this.folder.isEmpty()) {
			f.setFolderNum(0);
			this.folder.add(0, f);
		}else {
			f.setFolderNum(this.folder.get(this.folder.size()-1).getFolderNum()+1);
			this.folder.add(f);
		}
	}
	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	
	
	
}
